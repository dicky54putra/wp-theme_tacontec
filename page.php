<?php /* Template Name: Homepage */ ?>

<?php get_header() ?>

<?php get_template_part('components/_header'); ?>
<?php get_template_part('components/home/_home'); ?>
<?php get_template_part('components/_footer'); ?>

<?php get_footer(); ?>
