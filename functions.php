<?php

/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
/**
 * Enqueue custom scripts and styles.
 */
function main_script_enqueue()
{
	wp_enqueue_style(
		"seketpapat_custom_style",
		get_stylesheet_directory_uri() . "/dist/css/build.min.css",
		[],
		filemtime(get_template_directory() . "/dist/css/build.min.css")
	);
	wp_enqueue_script(
		"seketpapat_custom_script",
		get_template_directory_uri() . "/dist/js/build.min.js",
		["jquery"],
		filemtime(get_template_directory() . "/dist/js/build.min.js"),
		true
	);
}

add_action("wp_enqueue_scripts", "main_script_enqueue");

/**
 * Allow .svg upload
 */
function add_file_types_to_uploads($file_types)
{
	$new_filetypes = [];
	$new_filetypes["svg"] = "image/svg+xml";
	$file_types = array_merge($file_types, $new_filetypes);
	return $file_types;
}
add_filter("upload_mimes", "add_file_types_to_uploads");

/**
 * Register Site Settings
 */
require get_template_directory() . "/inc/site-settings.php";


add_theme_support('post-thumbnails');
