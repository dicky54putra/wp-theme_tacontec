<?php
$main_footer_menus = wp_get_nav_menu_items(MAIN_MENU);
$footer_menus = wp_get_nav_menu_items(SECOND_MENU);

$logo2      = get_field('ft-secondary_logo', SETTING_ID)['url'];

$email      = get_field('ft-email', SETTING_ID);
$address    = get_field('ft-address', SETTING_ID);

$facebook   = get_field('ft-facebook', SETTING_ID);
$phone      = get_field('ft-phone', SETTING_ID);
$whatsapp   = get_field('ft-whatsapp', SETTING_ID);
$instagram  = get_field('ft-instagram', SETTING_ID);
?>

<footer class="section full footer">
  <div class="container-flex">
    <div class="flex-col-12 sm:flex-col-12 md:flex-col-3">
      <img class="footer-logo" src="<?= $logo2 ?>" alt="Logo Footer">
      <ul class="footer-list mail-address">
        <li>
          <img class="footer-icon" src="<?= $email['icon'] ?>" alt="mail icon" srcset="">
          <a href="mailto:"><?= $email['value'] ?></a>
        </li>
        <li>
          <img class="footer-icon" src="<?= $address['icon'] ?>" alt="addres icon" srcset="">
          <span><?= $address['value'] ?></span>
        </li>
        <li>
          <div class="footer-icon"></div>
          <span><?= $address['value2'] ?></span>
        </li>
      </ul>
    </div>
    <div class="flex-col-12 sm:flex-col-4 md:flex-col-3">
      <h3 class="footer-title">Menu</h3>
      <ul class="footer-list">
        <?php foreach ($main_footer_menus as $key => $value) { ?>
          <li><a href="<?= $value->url ?>"><?= $value->post_title ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="flex-col-12 sm:flex-col-4 md:flex-col-3">
      <h3 class="footer-title">Our Service</h3>
      <ul class="footer-list">
        <?php foreach ($footer_menus as $key => $value) { ?>
          <li><a href="<?= $value->url ?>"><?= $value->post_title ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div class="flex-col-12 sm:flex-col-4 md:flex-col-3">
      <ul class="footer-list sosmed">
        <li>
          <img class="footer-icon" src="<?= $whatsapp['icon'] ?>" alt="" srcset="">
          <span><?= $whatsapp['value'] ?></span>
        </li>
        <li>
          <img class="footer-icon" src="<?= $phone['icon'] ?>" alt="" srcset="">
          <span><?= $phone['value'] ?></span>
        </li>
        <li>
          <img class="footer-icon" src="<?= $facebook['icon'] ?>" alt="" srcset="">
          <span><?= $facebook['value'] ?></span>
        </li>
        <li>
          <img class="footer-icon" src="<?= $instagram['icon'] ?>" alt="" srcset="">
          <span><?= $instagram['value'] ?></span>
        </li>
      </ul>
    </div>
  </div>
</footer>

<div class="credit">
  <div class="made">Made with</div>
  <div class="love">
    <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M10 17.9998L1.847 10.2398C0.656 9.10883 0 7.59983 0 5.99283C0 4.38583 0.656 2.87683 1.847 1.74283C4.067 -0.371173 7.552 -0.561173 10 1.16883C12.448 -0.561173 15.933 -0.371173 18.153 1.74183C19.344 2.87583 20 4.38483 20 5.99183C20 7.59883 19.344 9.10783 18.153 10.2408L10 17.9998Z" fill="#F04646" />
      <path fill-rule="evenodd" clip-rule="evenodd" d="M10 17L2.6623 10.1022C1.5904 9.09681 1 7.75547 1 6.32701C1 4.89855 1.5904 3.5572 2.6623 2.54919C4.6603 0.670065 7.7968 0.501175 10 2.03897C12.2032 0.501175 15.3397 0.670066 17.3377 2.54831C18.4096 3.55632 19 4.89766 19 6.32612C19 7.75458 18.4096 9.09592 17.3377 10.103L10 17Z" fill="url(#paint0_linear_110_6)" />
      <defs>
        <linearGradient id="paint0_linear_110_6" x1="10" y1="-3" x2="10" y2="11.5" gradientUnits="userSpaceOnUse">
          <stop stop-color="white" />
          <stop offset="1" stop-color="#F04646" stop-opacity="0" />
        </linearGradient>
      </defs>
    </svg>
  </div>
  <div class="made">by</div>
  <a href="https://dicky54putra.github.io">Dicky Saputra</a>
</div>