<?php
$title  = get_field('hero_title');
$desc   = get_field('hero_desc');
$image  = get_field('hero_image');
$btn    = get_field('hero_button');
$srcset = wp_get_attachment_image_srcset($image['ID'], 'medium');

?>
<section class="container-flex section hero">
  <div class="flex-col-12 md:flex-col-6 order-2 md:order-1">
    <h1 class="h1"><?= $title ?></h1>
    <p class="p1"><?= $desc ?></p>
    <a href="<?= $btn['url'] ?>" class="btn btn-primary"><?= $btn['title'] ?></a>
  </div>
  <div class="flex-col-12 md:flex-col-6 order-1 md:order-2">
    <figure class="image-wrapper right">
      <img src="<?= $image['url'] ?>" alt="<?= $image['title'] ?>" srcset="<?= $srcset ?>">
    </figure>
  </div>
</section>