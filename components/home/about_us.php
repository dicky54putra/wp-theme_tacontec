<?php
$title  = get_field('about_title');
$desc   = get_field('about_desc');
$image  = get_field('about_image');
$srcset = wp_get_attachment_image_srcset($image['ID'], 'medium');
?>
<div class="anchor" id="about-us"></div>
<section class="container-flex section about-us">
  <div class="flex-col-12 md:flex-col-6 order-2 md:order-2">
    <h2 class="h2"><?= $title ?></h2>
    <p class="p"><?= $desc ?></p>
  </div>
  <div class="flex-col-12 md:flex-col-6 order-1 md:order-1">
    <figure class="image-wrapper left">
      <img src="<?= $image['url'] ?>" alt="<?= $image['title'] ?>" srcset="<?= $srcset ?>">
    </figure>
  </div>
</section>