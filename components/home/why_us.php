<?php
$title  = get_field('why-us_title');
$image  = get_field('why-us_image');
$why_1  = get_field('why_us_1');
$why_2  = get_field('why_us_2');
$why_3  = get_field('why_us_3');
$why_4  = get_field('why_us_4');
$srcset = wp_get_attachment_image_srcset($image['ID'], 'medium');
?>
<section class="section full why-us">
  <div class="container-flex">
    <div class="flex-col-12 md:flex-col-6 order-2 md:order-1">
      <h2 class="h2"><?= $title ?></h2>
      <div class="flex why-us__wrapper">
        <?php
        for ($i = 1; $i <= 4; $i++) {
          $selector = "why_us_{$i}";
          $why_us = get_field($selector);
          $srcset = wp_get_attachment_image_srcset($why_us['image']['ID'], 'medium');
        ?>
          <div class="why-us__card">
            <img class="why-us__image" src="<?= $why_us['image']['url'] ?>" alt="<?= $why_us['image']['title'] ?>" srcset="<?= $srcset ?>">
            <div class="why-us__content">
              <h3 class="why-us__title"><?= $why_us['title'] ?></h3>
              <p class="why-us__desc"><?= $why_us['desc'] ?></p>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="flex-col-12 md:flex-col-6 order-1 md:order-2">
      <figure class="image-wrapper right">
        <img src="<?= $image['url'] ?>" alt="<?= $image['title'] ?>" srcset="<?= $srcset ?>">
      </figure>
    </div>
  </div>
</section>