<?php
$title  = get_field('our_service_title');
$srcset = wp_get_attachment_image_srcset($image['ID'], 'medium');
?>
<div class="anchor" id="our-services"></div>
<section class="container-flex section our-service">
  <div class="flex-col-12">
    <h2 class="h2 text-center"><?= $title ?></h2>
    <div class="flex our-service__wrapper">
      <?php
      for ($i = 1; $i <= 3; $i++) {
        $selector = "our_service_{$i}";
        $os = get_field($selector);
        $srcset = wp_get_attachment_image_srcset($os['image']['ID'], 'medium');
      ?>
        <div class="our-service__card">
          <img class="our-service__img" src="<?= $os['image']['url'] ?>" alt="<?= $os['image']['title'] ?>" srcset="<?= $srcset ?>">
          <h3 class="our-service__title"><?= $os['title'] ?></h3>
          <p class="our-service__desc"><?= $os['desc'] ?></p>
        </div>
      <?php } ?>
    </div>
  </div>
</section>