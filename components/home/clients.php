<?php
$srcset = wp_get_attachment_image_srcset($image['ID'], 'medium');

$clients_args = [
  'post_type'         => 'client',
  'post_status'       => 'publish',
  'posts_per_page'    => -1,
  'orderby'           => 'title',
  'order'             => 'ASC',

];
$clients = get_posts($clients_args);
?>
<div class="anchor" id="clients"></div>
<section id="clients" class="section clients">
  <div class="container-flex">
    <div class="flex-col-12">
      <div class="clients-img">
        <?php if ($clients) { ?>
          <?php
          if (count($clients) < 6) {
            $client_count = 6;
          } else {
            $client_count = count($clients);
          }
          ?>
          <?php for ($i = 0; $i < $client_count; $i++) { ?>
            <?php
            $count_client = count($clients);
            $num = $i % $count_client;
            $ID = $clients[$num]->ID;
            ?>
            <img class="item-img" src="<?= get_the_post_thumbnail_url($ID) ?>" />
          <?php } ?>
        <?php } ?>
      </div>
    </div>
  </div>
</section>