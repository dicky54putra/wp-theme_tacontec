<?php
$title  = get_field('our_product_title');
$desc   = get_field('our_product_desc');
$srcset = wp_get_attachment_image_srcset($image['ID'], 'medium');

$portfolio_args = [
  'post_type'         => 'portfolio',
  'post_status'       => 'publish',
  'posts_per_page'    => -1,
  'orderby'           => 'title',
  'order'             => 'ASC',

];
$portfolio = get_posts($portfolio_args);
?>
<div class="anchor" id="portofolio"></div>
<section class="section full our-product">
  <div class="container-flex">
    <div class="flex-col-12 md:flex-col-4">
      <h2 class="h2"><?= $title ?></h2>
      <p class="p"><?= $desc ?></p>
      <div class="slick-arrows">
        <div class="slick-arrows__left">
          <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16.5 7.46343L11.0059 13.1493L16.5 18.8352L14.8086 20.5818L7.61111 13.1493L14.8086 5.7168L16.5 7.46343Z" fill="black" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M13 1.5C6.64872 1.5 1.49999 6.64873 1.5 13C1.5 19.3513 6.64872 24.5 13 24.5C19.3513 24.5 24.5 19.3513 24.5 13C24.5 6.64872 19.3513 1.5 13 1.5ZM13 -2.6782e-06C5.82029 -2.05053e-06 -5.57886e-06 5.8203 -4.95119e-06 13C-4.32352e-06 20.1797 5.8203 26 13 26C20.1797 26 26 20.1797 26 13C26 5.8203 20.1797 -3.30587e-06 13 -2.6782e-06Z" fill="black" />
          </svg>
        </div>
        <div class="slick-arrows__right">
          <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.50003 18.8352L14.9941 13.1493L9.50003 7.46343L11.1914 5.7168L18.3889 13.1493L11.1914 20.5818L9.50003 18.8352Z" fill="black" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.0001 24.5C19.3513 24.5 24.5001 19.3513 24.5001 13C24.5001 6.64873 19.3513 1.5 13.0001 1.5C6.64879 1.5 1.50006 6.64873 1.50006 13C1.50006 19.3513 6.64879 24.5 13.0001 24.5ZM13.0001 26C20.1798 26 26.0001 20.1797 26.0001 13C26.0001 5.8203 20.1798 0 13.0001 0C5.82036 0 6.10352e-05 5.8203 6.10352e-05 13C6.10352e-05 20.1797 5.82036 26 13.0001 26Z" fill="black" />
          </svg>
        </div>
      </div>
    </div>
    <div class="flex-col-12 md:flex-col-8">
      <div class="port_slider">
        <?php if ($portfolio) { ?>
          <?php
          $port_count = count($portfolio);
          if ($port_count < 3) {
            $count_port = 3;
          } else {
            $count_port = $port_count;
          }
          ?>
          <?php for ($i = 0; $i < 5; $i++) { ?>
            <?php
            $num = $i % $port_count;
            $ID = $portfolio[$num]->ID;
            $title = $portfolio[$num]->post_title;
            ?>
            <div class="port_card">
              <img class="port_img" src="<?= get_the_post_thumbnail_url($ID) ?>" alt="" srcset="">
              <h3 class="h4"><?= $title ?></h3>
            </div>
          <?php } ?>
        <?php } ?>
      </div>
    </div>
  </div>
</section>