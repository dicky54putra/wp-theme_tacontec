<?php

?>

<main>
    <?php get_template_part('components/home/hero'); ?>
    <?php get_template_part('components/home/clients'); ?>
    <?php get_template_part('components/home/about_us'); ?>
    <?php get_template_part('components/home/why_us'); ?>
    <?php get_template_part('components/home/our_services'); ?>
    <?php get_template_part('components/home/our_products'); ?>
    <?php get_template_part('components/_cta'); ?>
</main>