<header id="header" class="header">
  <div class="container-flex">
    <div class="flex-col-6 md:flex-col-3 flex justify-start items-center">
      <a href="">
        <img class="logo" height="40" src="<?= get_template_directory_uri() . "/assets/images/logo.svg" ?>" alt="">
      </a>
    </div>
    <div class="flex-col main-menu">
      <div class="hamburger" id="hamburger">
        <svg width="30" height="19" viewBox="0 0 30 19" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="30" height="4.11765" rx="2.05882" fill="#30318B" />
          <rect x="10" y="7.05878" width="20" height="4.11765" rx="2.05882" fill="#30318B" />
          <rect x="5.29413" y="14.1177" width="24.7059" height="4.11765" rx="2.05882" fill="#30318B" />
        </svg>
      </div>
      <span class="menu-close" id="menu-close">
        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect y="15.8035" width="22.3496" height="3.06759" rx="1.53379" transform="rotate(-45 0 15.8035)" fill="#30318B" />
          <rect x="2.19113" y="0.0273438" width="22.3496" height="3.06759" rx="1.53379" transform="rotate(45 2.19113 0.0273438)" fill="#30318B" />
        </svg>
      </span>
      <?php wp_nav_menu(); ?>
    </div>
</header>