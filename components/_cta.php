<?php
$text  = get_field('cta_text');
$btn  = get_field('cta_button');
?>
<div class="anchor" id="contact-us"></div>
<section class="container-flex section full cta">
  <div class="flex-col-12 text-center flex">
    <h2 class="h3"><?= $text ?></h2>
    <a href="<?= $btn['url'] ?>" class="btn btn-primary"><?= $btn['title'] ?></a>
  </div>
</section>