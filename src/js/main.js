import $, { htmlPrefilter, ajax } from "jquery";
window.$ = window.jQuery = $;
import "slick-carousel";

(function ($) {
  let slickCarouselButton = (btnClass, element, options) => {
    $(btnClass).click(function () {
      $(element).slick(options);
    });
  };

  slickCarouselButton(".slick-arrows__left", ".port_slider", "slickPrev");
  slickCarouselButton(".slick-arrows__right", ".port_slider", "slickNext");

  $(".port_slider").slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    // variableWidth: true,
    responsive: [
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  $(".clients-img").slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    // variableWidth: true,
    responsive: [
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
    ],
  });
})(jQuery);

function useScrollAnchor() {
  const smoothScrollAnchor = document.querySelectorAll("a[href^='#']");

  for (let anchor = 0; anchor < smoothScrollAnchor.length; anchor++) {
    const element = smoothScrollAnchor[anchor];

    element.addEventListener("click", function (e) {
      e.preventDefault();
      if (document.getElementById(this.getAttribute("href").replace("#", "")))
        document.querySelector(this.getAttribute("href")).scrollIntoView({
          behavior: "smooth",
        });
    });
  }
}
useScrollAnchor();

function openMenuMobile() {
  const hamburger = document.getElementById("hamburger");
  const close = document.getElementById("menu-close");
  const bgMenu = document.querySelector(".menu-main-menu-container");

  hamburger.onclick = () => {
    close.classList.add("active");
    bgMenu.classList.add("active");
  };

  bgMenu.onclick = () => {
    close.classList.remove("active");
    bgMenu.classList.remove("active");
  };
  close.onclick = () => {
    close.classList.remove("active");
    bgMenu.classList.remove("active");
  };
}

openMenuMobile();
